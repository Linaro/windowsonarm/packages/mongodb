#!/usr/bin/env bash

set -euo pipefail
set -x

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_dir"
version=$1; shift
out_dir=$1; shift

PYTHON_VENV=null

# delete remaining bazel
trap "cmd /c 'taskkill.exe /f /im java.exe'" EXIT

checkout()
{
    if [ -d mongodb ]; then
        return
    fi
    # clone from our fork
    git clone https://gitlab.com/Linaro/windowsonarm/forks/mongodb mongodb
    pushd mongodb
    git reset --hard $version
    git log -n1
    git submodule update --init
    popd
}

create_python_venv()
{
    export PYTHON_VENV=$(pwd)/venv
    if [ -d venv ]; then
        return
    fi
    python3 -m venv venv
}

venv()
{
    cmd /c "$(cygpath -m $PYTHON_VENV)/Scripts/activate.bat && $*"
}

python_pip_deps()
{
    venv python -m pip install --upgrade pip
    # those deps were found by running build
    venv python -m pip install poetry pyyaml retry pywin32 Cheetah3
}

dep_openssl()
{
    export LIB="$LIB;$(cygpath -m $(pwd))/openssl/"
    export INCLUDE="$INCLUDE;$(cygpath -m $(pwd))/openssl/include"
    if [ -f openssl/.done ]; then
        echo "openssl built"
        return
    fi
    rm -rf openssl
    git clone https://github.com/openssl/openssl
    pushd openssl
    perl Configure VC-WIN64-ARM
    nmake
    popd
    touch openssl/.done
}

dep_libsodium()
{
    export LIB="$LIB;$(cygpath -n $(pwd))/libsodium/bin/ARM64/Release/v143/static/"
    export INCLUDE="$INCLUDE;$(cygpath -n $(pwd))/libsodium/src/libsodium/include/"
    export SODIUM_INSTALL=system
    export PYNACL_SODIUM_STATIC=1
    if [ -f libsodium/.done ]; then
        echo "libsodium built"
        return
    fi
    rm -rf libsodium
    git clone https://github.com/jedisct1/libsodium
    pushd libsodium
    msbuild.exe /p:Configuration=StaticRelease /p:Platform=ARM64  builds/msvc/vs2022/libsodium.sln
    popd
    touch libsodium/.done
}

dep_mongo_ninja()
{
    # wheel is not available from arm64, so craft it from x64.
    # It just uses a precompiled ninja binary, and pure python code to call it.
    # https://pypi.org/project/mongo-ninja-python/#files
    # poetry lock file was modified to use this
    local version=mongo_ninja_python-1.11.1.7

    if [ -f $version-py2.py3-none-win_arm64.whl ]; then
        echo "mongo_ninja_python arm64 wheel already created"
        return
    fi

    wheel_folder=mongo_ninja_python_wheel
    rm -rf $wheel_folder
    mkdir $wheel_folder
    pushd $wheel_folder
    wget https://files.pythonhosted.org/packages/a3/f6/90d351faba07b8288eb2ae537a72b47ecbf78a53e281f56eb16707b07910/$version-py2.py3-none-win_amd64.whl
    unzip $version-py2.py3-none-win_amd64.whl
    rm *.whl
    sed -e 's/amd64/arm64/' $version.dist-info/WHEEL
    # skip timestamps
    zip -X ../$version-py2.py3-none-win_arm64.whl -r ./
    popd
    rm -rf $wheel_folder
    venv python3 -m pip install $version-py2.py3-none-win_arm64.whl
}

python_poetry_deps()
{
    export PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring
    pushd mongodb
    if [ ! -f poetry_lock_updated ]; then
        venv python3 -m poetry lock --no-update
        touch poetry_lock_updated
    fi
    venv python3 -m poetry install --no-root --sync
    popd
}

configure()
{
    # delete bazel stuff
    rm -rf "$USERPROFILE/_bazel_user"
    pushd mongodb
    if [ ! -f configured ]; then
        venv python3 buildscripts/scons.py install-mongod  TARGET_ARCH=aarch64 HOST_ARCH=aarch64 CC=cl.exe CXX=cl.exe --ninja=enabled
        touch configured
    fi
    popd
}

build()
{
    pushd mongodb
    venv ninja -j1 -v
    popd
}

checkout
pacman -Sy --noconfirm --needed zip
create_python_venv
python_pip_deps
dep_openssl
dep_libsodium
dep_mongo_ninja
python_poetry_deps

configure
build
